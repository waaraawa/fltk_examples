#if defined(WIN32) && !defined(__CYGWIN__)
    #define _WIN32_WINNT 0x0501 // needed for AttachConsole
    #include <windows.h> // AllocConsole()
    #include <Wincon.h> // AttachConsole()
#endif

#include <FL/Fl.H>
#include <FL/Fl_Window.H>
#include <FL/Fl_Box.H>
#include <FL/Fl_Input.H>
#include <FL/Fl_PNM_Image.H>
#include <FL/Fl_Light_Button.H>
#include <FL/Fl_Double_Window.H>
#include <FL/Fl_Text_Buffer.H>
#include <FL/Fl_Text_Display.H>
#include <FL/filename.H>
#include <FL/Fl_PNG_Image.H>
#include <FL/Fl_Choice.H>
#include <FL/Fl_Tree.H>
#include <string.h>
#include <stdio.h>

#define MAIN_WIDTH 800
#define MAIN_HEIGHT 600

Fl_Text_Display *files;
Fl_Text_Buffer *files_buf;

void cb_btn(Fl_Widget* w, void* p) {
    char tmp[256] = {0,};
    sprintf(tmp, "btn: %d, %s\n", ((Fl_Choice*)p)->value(), ((Fl_Choice*)p)->text());
    files->insert(tmp);
    files->show_insert_position();
}

void cb_fourth(Fl_Widget* w, void* p) {
    char tmp[256] = {0,};
    sprintf(tmp, "cb_fourth\n", ((Fl_Choice*)p)->value(), ((Fl_Choice*)p)->text());
    files->insert(tmp);
    files->show_insert_position();
}

void TreeCallback(Fl_Widget *w, void *data) {
    char tmp[256] = {0,};
    Fl_Tree *tree = (Fl_Tree*)w;
    printf("TreeCallback: data=%d, ", (int)data);
    // Find item that was clicked
    Fl_Tree_Item *item = (Fl_Tree_Item*)tree->item_clicked();

    switch ( tree->callback_reason() ) {
    case FL_TREE_REASON_SELECTED:
        printf("item selected: %s\n", item->label());
        sprintf(tmp, "item selected: %s\n", item->label());
        break;
    case FL_TREE_REASON_DESELECTED:
        printf("item deselected: %s\n", item->label());
        sprintf(tmp, "item deselected: %s\n", item->label());
        break;
    case FL_TREE_REASON_OPENED:
        printf("item opened: %s\n", item->label());
        sprintf(tmp, "item opened: %s\n", item->label());
        break;
    case FL_TREE_REASON_CLOSED:
        printf("item closed: %s\n", item->label());
        sprintf(tmp, "item closed: %s\n", item->label());
        break;
    default:
        break;
    }

    files->insert(tmp);
    files->show_insert_position();
}

int main(int argc, char **argv) {
#if defined(WIN32) && !defined(__CYGWIN__)
    // Open a DOS style console, redirect stdout to it
    AllocConsole() ;
    AttachConsole(GetCurrentProcessId()) ;
    freopen("CON", "w", stdout) ;
    freopen("CON", "w", stderr) ;

    printf("Hello world on stdout!\n");
    printf("Hello world on stderr!\n");
#endif

    Fl_Window *window = new Fl_Window(MAIN_WIDTH, MAIN_HEIGHT);
    Fl_Box *box = new Fl_Box(10, 10, 200, 20, "Fl_Choice Test");
    Fl_Choice *choice;
    Fl_Button *btn;

    Fl::scheme(NULL);

    choice = new Fl_Choice(50, 60, 140, 20, "Choice");
    choice->add("first", "^1", NULL);
    choice->add("second", "^2", NULL);
    choice->add("third", "^3", NULL);
    choice->add("fourth", "^4", cb_fourth, choice);
    choice->value(0);

    btn = new Fl_Button(200, 60, 20, 20);
    btn->labelcolor(FL_RED);
    btn->image(new Fl_PNG_Image("./check_icon_20_none.png"));
    btn->callback(cb_btn, choice);

    files = new Fl_Text_Display(10, 90, MAIN_WIDTH - 20, 200);
    files_buf = new Fl_Text_Buffer();
    files->buffer(files_buf);

    Fl_Tree tree(10, 300, MAIN_WIDTH - 20, MAIN_HEIGHT - 300 - 20);
    tree.begin();
    tree.add("Flintstones/Fred");
    tree.add("Flintstones/Wilma");
    tree.add("Flintstones/Pebbles");
    tree.add("Simpsons/Homer");
    tree.add("Simpsons/Marge");
    tree.add("Simpsons/Bart");
    tree.add("Simpsons/Lisa");
    tree.end();
    tree.callback(TreeCallback, (void*)1234);

    window->end();
    window->show(argc, argv);
    return Fl::run();
}
