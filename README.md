#FLTK examples

##Tree
Fl_Button: png image button, change button image, Fl_Button callback, Fl_Input callback  
Fl_Choice: dropdown menu, shortcut, Fl_Choice callback
Fl_Tree: tree widget, print log on console, Fl_Tree callback

##History
2017-11-23, Add Fl_Tree folder
2017-11-22, Add Fl_Choice folder  
2017-11-21, Add Fl_Button folder
