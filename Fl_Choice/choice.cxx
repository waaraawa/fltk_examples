#include <FL/Fl.H>
#include <FL/Fl_Window.H>
#include <FL/Fl_Box.H>
#include <FL/Fl_Input.H>
#include <FL/Fl_PNM_Image.H>
#include <FL/Fl_Light_Button.H>
#include <FL/Fl_Double_Window.H>
#include <FL/Fl_Text_Buffer.H>
#include <FL/Fl_Text_Display.H>
#include <FL/filename.H>
#include <FL/Fl_PNG_Image.H>
#include <FL/Fl_Choice.H>
#include <string.h>
#include <stdio.h>

#define MAIN_WIDTH 800
#define MAIN_HEIGHT 600

Fl_Text_Display *files;
Fl_Text_Buffer *files_buf;

void cb_btn(Fl_Widget* w, void* p) {
    char tmp[256] = {0,};
    sprintf(tmp, "btn: %d, %s\n", ((Fl_Choice*)p)->value(), ((Fl_Choice*)p)->text());
    files->insert(tmp);
}

void cb_fourth(Fl_Widget* w, void* p) {
    char tmp[256] = {0,};
    sprintf(tmp, "cb_fourth\n", ((Fl_Choice*)p)->value(), ((Fl_Choice*)p)->text());
    files->insert(tmp);
}

int main(int argc, char **argv) {
    Fl_Window *window = new Fl_Window(MAIN_WIDTH, MAIN_HEIGHT);
    Fl_Box *box = new Fl_Box(10, 10, 200, 20, "Fl_Choice Test");
    Fl_Choice *choice;
    Fl_Button *btn;

    Fl::scheme(NULL);

    choice = new Fl_Choice(50, 60, 140, 20, "Choice");
    choice->add("first", "^1", NULL);
    choice->add("second", "^2", NULL);
    choice->add("third", "^3", NULL);
    choice->add("fourth", "^4", cb_fourth, choice);
    choice->value(0);

    btn = new Fl_Button(200, 60, 20, 20);
    btn->labelcolor(FL_RED);
    btn->image(new Fl_PNG_Image("./check_icon_20_none.png"));
    btn->callback(cb_btn, choice);

    files = new Fl_Text_Display(10, 90, MAIN_WIDTH - 20, 200);
    files_buf = new Fl_Text_Buffer();
    files->buffer(files_buf);

    window->end();
    window->show(argc, argv);
    return Fl::run();
}
