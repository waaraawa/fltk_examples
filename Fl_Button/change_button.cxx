#include <FL/Fl.H>
#include <FL/Fl_Window.H>
#include <FL/Fl_Box.H>
#include <FL/Fl_Input.H>
#include <FL/Fl_PNM_Image.H>
#include <FL/Fl_Light_Button.H>
#include <FL/Fl_Double_Window.H>
#include <FL/Fl_Text_Buffer.H>
#include <FL/Fl_Text_Display.H>
#include <FL/filename.H>
#include <FL/Fl_PNG_Image.H>
#include <string.h>
#include <stdio.h>

#define MAIN_WIDTH 800
#define MAIN_HEIGHT 600

Fl_Input *txt;
Fl_Text_Display *files;
Fl_Text_Buffer *files_buf;

void cb_txt_changed(Fl_Widget* w, void* p);
void cb_check_txt_len(Fl_Widget* w, void* p);

int main(int argc, char **argv) {
    Fl_Window *window = new Fl_Window(MAIN_WIDTH, MAIN_HEIGHT);
    Fl_Box *box = new Fl_Box(10, 10, 200, 20, "Fl_Button Test");

    Fl_Button *btn;

    Fl::scheme(NULL);

    btn = new Fl_Button(200, 60, 20, 20);
    btn->labelcolor(FL_RED);
    btn->image(new Fl_PNG_Image("./check_icon_20_none.png"));
    btn->callback(cb_check_txt_len, btn);

    txt = new Fl_Input(50, 60, 140, 20, "TXT: ");
    txt->value("0123456789");
    txt->callback(cb_txt_changed, btn);
    txt->when(FL_WHEN_CHANGED);

    files = new Fl_Text_Display(10, 90, MAIN_WIDTH - 20, 200);
    files_buf = new Fl_Text_Buffer();
    files->buffer(files_buf);

    window->end();
    window->show(argc, argv);
    return Fl::run();
}

void cb_txt_changed(Fl_Widget* w, void* p) {
    char tmp[FL_PATH_MAX];
    sprintf(tmp, "cb_txt_changed++, %p\n", p);
    files->insert(tmp);
    ((Fl_Button*)p)->image(new Fl_PNG_Image("./check_icon_20_none.png"));
    ((Fl_Button*)p)->redraw();
}

void cb_check_txt_len(Fl_Widget* w, void* p) {
    const char *in_txt = txt->value();

    char tmp[256] = {0,};
    sprintf(tmp, "strlen: %d, %p\n", strlen(in_txt), p);
    files->insert(tmp);

    if (10 <= strlen(in_txt)) {
        ((Fl_Button*)p)->image(new Fl_PNG_Image("./check_icon_20_true.png"));
    } else {
        ((Fl_Button*)p)->image(new Fl_PNG_Image("./check_icon_20_false.png"));
    }

    ((Fl_Button*)p)->redraw();
    txt->redraw();
}
